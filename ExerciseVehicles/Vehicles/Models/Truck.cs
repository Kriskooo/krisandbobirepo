﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vehicles.Models
{
    public class Truck : Vehicle
    {
        private const double increaseFuelConsumption = 1.6;

        public Truck(double fuelQuantity, double fuelConsumption) 
            : base(fuelQuantity, fuelConsumption)
        {
        }

        public override void Drive(double kilometres)
        {
            if (this.FuelQuantity - ((this.FuelConsumption + increaseFuelConsumption) * kilometres) >= 0)
            {
                this.FuelQuantity -= (this.FuelConsumption + increaseFuelConsumption) * kilometres;
                Console.WriteLine($"{this.GetType().Name} travelled {kilometres} km");
            }
            else
            {
                Console.WriteLine($"{this.GetType().Name} needs refueling");

            }
        }

        public override void Refuel(double refuelQuantity)
        {
            this.FuelQuantity += refuelQuantity * 95 / 100;
        }

    }
}
