﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vehicles.Models
{
    public class Car : Vehicle
    {
        private const double increaseFuelConsumption = 0.9;
        public Car(double fuelQuantity, double fuelConsumption)
            : base(fuelQuantity, fuelConsumption)
        {
        }

        public override void Drive(double kilometres)
        {
            if (this.FuelQuantity - ((this.FuelConsumption + increaseFuelConsumption) * kilometres) >= 0)
            {
                this.FuelQuantity -= (this.FuelConsumption + increaseFuelConsumption) * kilometres;
                Console.WriteLine($"{this.GetType().Name} travelled {kilometres} km");
            }
            else
            {
                Console.WriteLine($"{this.GetType().Name} needs refueling");
            }
        }
    }
}
