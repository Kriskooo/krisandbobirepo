﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vehicles.Models
{
    public abstract class Vehicle
    {
       // private double fuelQuantity;
       // private double fuelConsumption;

        protected Vehicle(double fuelQuantity, double fuelConsumption)
        {
            FuelQuantity = fuelQuantity;
            FuelConsumption = fuelConsumption;
        }

        public double FuelQuantity { get; set; }
        public double FuelConsumption { get; set; }

        public abstract void Drive(double kilometres);

        public virtual void Refuel(double refuelQuantity)
        {
            this.FuelQuantity += refuelQuantity;
        }

    }
}
