﻿using System;
using Vehicles.Core;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Vehicles.Models;


namespace Vehicles
{
    public class StartUp
    {
        static void Main(string[] args)
        {
            string carInfo = Console.ReadLine();
            string[] carInfoTokens = carInfo
                .Split()
                .ToArray();

            double carFuelQuantity = double.Parse(carInfoTokens[1]);
            double carLittersKm = double.Parse(carInfoTokens[2]);
            Vehicle car = new Car(carFuelQuantity, carLittersKm);

            string truckInfo = Console.ReadLine();
            string[] truckInfoTokens = truckInfo
                .Split()
                .ToArray();

            double truckFuelQuantity = double.Parse(truckInfoTokens[1]);
            double truckLittersKm = double.Parse(truckInfoTokens[2]);
            Vehicle truck = new Truck(truckFuelQuantity, truckLittersKm);

            int nCommands = int.Parse(Console.ReadLine());

            for (int i = 0; i < nCommands; i++)
            {
                string command = Console.ReadLine();

                string[] commandTokens = command
                    .Split()
                    .ToArray();

                string action = commandTokens[0];
                string vechicleType = commandTokens[1];

                if (vechicleType == "Car")
                {
                    if (action == "Drive")
                    {
                        double distance = double.Parse(commandTokens[2]);
                        car.Drive(distance);
                    }
                    else
                    {
                        double quantity = double.Parse(commandTokens[2]);
                        car.Refuel(quantity);
                    }
                }
                else if (vechicleType == "Truck")
                {
                    if (action == "Drive")
                    {
                        double distance = double.Parse(commandTokens[2]);
                        truck.Drive(distance);
                    }
                    else
                    {
                        double quantity = double.Parse(commandTokens[2]);
                        truck.Refuel(quantity);
                    }
                }
            }

            Console.WriteLine($"Car: {car.FuelQuantity:F2}");
            Console.WriteLine($"Truck: {truck.FuelQuantity:F2}");
        }
    }
}
